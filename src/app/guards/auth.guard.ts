import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { loggedIn } from '../../firebase/firebase.utils';
import { AngularFireAuth } from '@angular/fire/auth';
//Esto sirve para restringir al usuario según las condiciones que queramos

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router,
    private AFauth: AngularFireAuth
    ){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      //Bug funcion async no recibe si hay un usuario logeado
      if (loggedIn()){
        this.AFauth.onAuthStateChanged(user=>{
          console.log(user);
        })
        return true
      }
      
      
      return true
  }
  
}
