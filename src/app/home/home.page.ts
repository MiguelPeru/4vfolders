import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public mobile: boolean;

  constructor() { }

  ngOnInit() {
    if (window.screen.width < 380) {
      this.mobile = true;
    } else {
      this.mobile = false;
    }

    switch (location.hash) {
      case "#entrevistaMaialen":
        this.scrollTo("entrevistaMaialen");
        break;
      case "#guia":
        this.scrollTo("guia");
        break;
    }
  }
  scrollTo(hash: string) {
    setTimeout(() => {
      let el = document.getElementById(hash);
      el.scrollIntoView();
    }, 300);
  }
  scroll(){
    document.querySelector('#guia').scrollIntoView({behavior:'smooth'});
  }
  scroll2(){
    document.querySelector('#guiarapida').scrollIntoView({behavior:'smooth'});
  }
}
