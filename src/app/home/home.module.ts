import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { TwitterComponent } from '../components/twitter/twitter.component';
import { FacebookComponent } from '../components/facebook/facebook.component';
import { NgxTwitterTimelineModule } from 'ngx-twitter-timeline';
import { MDBRootModule } from 'angular-bootstrap-md';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    NgxTwitterTimelineModule,
    MDBRootModule,
  ],
  declarations: [HomePage, TwitterComponent, FacebookComponent]
})
export class HomePageModule {}
