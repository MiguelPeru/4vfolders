import { Component, OnInit } from '@angular/core';
import { CommentsService } from 'src/app/shared/comentarios.service';
import { AuthenticationService } from 'src/app/shared/authentication-service';
import { Comentario } from 'src/app/shared/comentario';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { ModalCommentPage } from 'src/app/modal-comment/modal-comment.page';
@Component({
  selector: 'app-comentarios',
  templateUrl: './comentarios.component.html',
  styleUrls: ['./comentarios.component.scss'],
})
export class ComentariosComponent implements OnInit {
  public spinner: boolean = false;
  public content: String;
  public loadMore: number;
  public comentarios: Comentario[]= []
  constructor(
    private comentariosService: CommentsService,
    public authenticationService: AuthenticationService,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController
    ) { }
  loadMoreComments(){
    this.spinner = true;
    setTimeout(()=>{
      this.spinner=false;
    this.loadMore -= 3;
    },700)
    
  }
  ngOnInit() {
    this.getComentarios()
  }
  enviarComentario(){
    let comentario: any;
    comentario = {
      userName: this.authenticationService.userData.displayName,
      content: this.content,
      date: new Date().getTime(),
      userId: this.authenticationService.userData.uid,
      subComments: [],
    }
    this.comentariosService.postComment(comentario);
    this.content = '';
  }
  getComentarios(){
    this.comentariosService.getComments().subscribe( data=> {
      this.comentarios = data.map(e => {
        return {
          uid: e.payload.doc.id,
          userName: e.payload.doc.data()['userName'],
          content: e.payload.doc.data()['content'],
          date: e.payload.doc.data()['date'],
          userId: e.payload.doc.data()['userId'],
          subComments: e.payload.doc.data()['subComments']
        }
        
      })
      this.comentarios.sort((a, b)=>(a.date > b.date) ? 1 : -1)
      this.loadMore = this.comentarios.length - 7;
      console.log(this.comentarios)
    })
  }
  async commentTapped(event, comentario){
    const modal = await this.modalController.create({
      component: ModalCommentPage,
      componentProps: {
        comment: comentario
      },
    });
    modal.present();
  }
  dateStampToDate(datestamp){
    let date = new Date(datestamp * 1000);
    let hours = date.getHours();
    let minutes = "0"+ date.getMinutes();
    let seconds = "0" + date.getSeconds();
    let formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    console.log(formattedTime);
    return formattedTime;

  }
  async borrarComentario(event, comentario) {
    const actionSheet = await this.actionSheetController.create({
      header: '¿Seguro que quieres borrar el comentario?',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.comentariosService.borrarComentario(comentario.uid);
          console.log('Delete clicked', comentario);
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  async borrarSubComentario(event, comentario, subComentario){
    const actionSheet = await this.actionSheetController.create({
      header: '¿Seguro que quieres borrar el comentario?',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.comentariosService.borrarSubComentario(comentario, subComentario);
          console.log('Delete clicked');
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
}
