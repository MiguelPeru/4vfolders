import { Component, OnInit, Input } from '@angular/core';
import { Team } from 'src/app/shared/statistics';

@Component({
  selector: 'app-info-statistics',
  templateUrl: './info-statistics.component.html',
  styleUrls: ['./info-statistics.component.scss'],
})
export class InfoStatisticsComponent implements OnInit {
  @Input() topFiveTeams: Team[];
  constructor() { }

  ngOnInit() {
    
  }
}
