import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/shared/authentication-service';
import { Router } from '@angular/router';
import { auth } from '../../../firebase/firebase.utils';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
  constructor(
    public authService: AuthenticationService,
    public router: Router,
    public alertCtrl: AlertController
  ) { }

  ngOnInit() { }

  signUp(email, password, leyes, condiciones, displayName) {
    if (leyes.checked == true) {
      if (condiciones.checked == true) {
        this.authService.RegisterUser(email.value, password.value)
          .then((res) => {
            // Do something here
            const currentUser = {
              ...res.user,
              displayName: displayName.value,
              password: password.value,
            }
            this.authService.SetUserData(currentUser);
            auth.currentUser.updateProfile({
              displayName: displayName.value
            })
            this.authService.SendVerificationMail()
            this.router.navigate(['verify-email']);
          }).catch((error) => {
            this.showAlert(error.message);
          })
      } else {
        this.showAlert("Debes acceptar las condiciones de servicio.");
      }
    } else {
      this.showAlert("Debes acceptar la LOPD, Cookies y RGPD.");
    }

  }

  async showAlert(message: string) {
    const alert = await this.alertCtrl.create({
      header: 'Atención',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
    const result = await alert.onDidDismiss();
    console.log(result);
  }
}