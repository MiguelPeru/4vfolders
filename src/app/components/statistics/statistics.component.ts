import { Component, OnInit } from '@angular/core';
import { StatisticsService } from 'src/app/shared/statistics.service';
import { Donor, Team } from 'src/app/shared/statistics';
import { isUndefined } from 'util';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss'],
})
//Para usar este componente es necesario añadir al array de imports del modulo de la página en la que este se encuantre los siquientes modulos: FormsModule, NgxDatatableModule
//import { NgxDatatableModule } from '@swimlane/ngx-datatable';
//import { FormsModule } from '@angular/forms';
export class StatisticsComponent implements OnInit {
  public search: String;
  public teams: Team[] = [];
  public topFiveTeams: Team[] = [];
  public tableStyle = 'bootstrap';
  public tableColor = 'Claro';
  public top10donors4v: Donor[];
  constructor(private statisticService: StatisticsService) { }

  ngOnInit() {
    this.getTeams(10);
    this.get4VDonors();
  }
  getDonors() {
    this.statisticService.getDonors().subscribe(response => {
      console.log(response);
      this.teams = response.results;
    })
  }
  //Funcion que recoge todos los datos de equipos
  getDataOfTeams(teams: Team[]) {
    let teamsData: Team[] = [];
    teams.forEach(team => {
      this.statisticService.getTeam(team.team).subscribe(response => {
        teamsData.push(response);
        // Los ordenamos por puesto.
        this.topFiveTeams.sort(this.compareTeams);
      });
    });
    return teamsData;
  }
  switchStyle() {
    if (this.tableStyle == 'dark') {
      this.tableStyle = 'bootstrap';
      this.tableColor = 'Claro'
    } else {
      this.tableStyle = 'dark';
      this.tableColor = 'Oscuro'
    }
  }
  //top teams
  getTeams(numTeams: number) {
    this.statisticService.getTeams().subscribe(response => {
      console.log(response);
      this.teams = response.results.slice(0, numTeams);
      //Siempre que se actualice el top se actualizará el top 5
      //Cogemos los 5 primeros del top
      let top5: Team[] = this.teams.slice(0, 5);
      //y buscamos todos los datos ya que en el top solo muestra los datos básicos.
      this.topFiveTeams = this.getDataOfTeams(top5);
      console.log('Top 5:', this.topFiveTeams);
    });
  }
  //Members of a clicked team
  donorsOfTeam(event) {
    if (event.type == 'click') {
      console.log(event.row);
      if (!isUndefined(event.row.donors)) {
        this.teams = event.row.donors;
      } else {
        this.statisticService.getTeam(event.row.team).subscribe(response => {
          console.log(response);
          this.teams = response.donors
        })
      }

    }
  }
  getTeam() {
    this.statisticService.getTeam(this.search).subscribe(response => {
      console.log(response);
      let teamsArray: Team[] = [];
      teamsArray.push(response);
      this.teams = teamsArray;
    })
  }
  get4VDonors() {
    this.statisticService.getTeam("244686").subscribe(response => {
      let team4v: Team = response;
      let donors4v: Donor[] = team4v.donors;
      this.top10donors4v = donors4v.slice(0, 10);
    })
  }
  compareTeams(a: Team, b: Team) {
    if (a.rank < b.rank) {
      return -1;
    }
    if (a.rank > b.rank) {
      return 1;
    }
    return 0;
  }
}
