import { Component, OnInit } from '@angular/core';
import { signInWithGoogle } from '../../../firebase/firebase.utils';
import { AuthenticationService } from 'src/app/shared/authentication-service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit {

  SignInWithGoogle() {
    signInWithGoogle();
  }
  constructor(
    public authService: AuthenticationService,
    public router: Router,
    public alertCtrl: AlertController
  ) { }

  ngOnInit() { }

  signIn(email, password) {
    this.authService.SignIn(email.value, password.value)
      .then((res) => {
        if (res.user.emailVerified) {
          this.router.navigate(['/tablon']);
        } else {
          this.showAlert("El correo no está validado.");
          return false;
        }
      }).catch((error) => {
        this.showAlert(error.message);
      })
  }

  async showAlert(message: string) {
    const alert = await this.alertCtrl.create({
      header: 'Atención',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
    const result = await alert.onDidDismiss();
    console.log(result);
  }
}
