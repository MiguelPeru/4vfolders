import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignInAndSignUpPage } from './sign-in-and-sign-up.page';

const routes: Routes = [
  {
    path: '',
    component: SignInAndSignUpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignInAndSignUpPageRoutingModule {}
