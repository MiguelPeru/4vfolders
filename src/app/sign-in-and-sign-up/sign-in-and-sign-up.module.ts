import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SignInAndSignUpPageRoutingModule } from './sign-in-and-sign-up-routing.module';

import { SignInAndSignUpPage } from './sign-in-and-sign-up.page';
import { SignInComponent } from '../components/sign-in/sign-in.component';
import { SignUpComponent } from '../components/sign-up/sign-up.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignInAndSignUpPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [SignInAndSignUpPage, SignInComponent, SignUpComponent]
})
export class SignInAndSignUpPageModule {}
