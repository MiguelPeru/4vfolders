import { Component } from '@angular/core';
import { AuthenticationService } from './shared/authentication-service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  public mobile: boolean;

  constructor(
    public authService: AuthenticationService,
  ) {}

  initializeApp() {
    
  }

  ngOnInit() {
    if (window.screen.width < 500) {
      this.mobile = true;
    } else {
      this.mobile = false;
    }
  }

  signOut(){
    this.authService.SignOut();
  }
}
