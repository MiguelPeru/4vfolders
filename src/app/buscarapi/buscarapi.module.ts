import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuscarapiPageRoutingModule } from './buscarapi-routing.module';
import { StatisticsComponent } from '../components/statistics/statistics.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { BuscarapiPage } from './buscarapi.page';
import { InfoStatisticsComponent } from '../components/info-statistics/info-statistics.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuscarapiPageRoutingModule,
    NgxDatatableModule
  ],
  declarations: [BuscarapiPage, StatisticsComponent, InfoStatisticsComponent]
})
export class BuscarapiPageModule {}
