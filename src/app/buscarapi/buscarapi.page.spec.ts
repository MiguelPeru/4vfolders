import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuscarapiPage } from './buscarapi.page';

describe('BuscarapiPage', () => {
  let component: BuscarapiPage;
  let fixture: ComponentFixture<BuscarapiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscarapiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuscarapiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
