import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuscarapiPage } from './buscarapi.page';

const routes: Routes = [
  {
    path: '',
    component: BuscarapiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuscarapiPageRoutingModule {}
