import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeyesPage } from './leyes.page';

const routes: Routes = [
  {
    path: '',
    component: LeyesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeyesPageRoutingModule {}
