import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LeyesPageRoutingModule } from './leyes-routing.module';

import { LeyesPage } from './leyes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LeyesPageRoutingModule
  ],
  declarations: [LeyesPage]
})
export class LeyesPageModule {}
