import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LeyesPage } from './leyes.page';

describe('LeyesPage', () => {
  let component: LeyesPage;
  let fixture: ComponentFixture<LeyesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeyesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LeyesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
