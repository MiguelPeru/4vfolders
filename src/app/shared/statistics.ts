export interface Donor {
    id: Number,
    credit_cert: String,
    name: String,
    rank: Number,
    credit: Number,
    team: Number,
    wus_cert: String,
    wus: Number
}

export interface Team {
    active_50: Number,
    credit: Number,
    credit_cert: String,
    donors: Donor[],
    last: Date,
    logo: String,
    name: String,
    path: String,
    rank: Number,
    team: Number,
    total_teams: Number,
    url: String,
    wus: Number,
    wus_cert: String,
    id: Number,
}