export interface Comentario{
    uid: string;
    userName: string;
    content: string;
    date: number;
    subComments: Comentario[];
}