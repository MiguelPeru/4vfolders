import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable , of, throwError } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class StatisticsService{
    private curl = `https://stats.foldingathome.org/api/`;
    private cors = 'https://cors-anywhere.herokuapp.com/';
    private donors = 'donors';
    private teams = 'teams';
    private team = 'team';
    constructor(private http: HttpClient){}
    
    getDonors(): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.get(this.cors+this.curl+this.donors, {headers: headers});
    }
    getTeams(): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.get(this.cors+this.curl+this.teams, {headers: headers});
    }
    getTeam(idTeam): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.get(this.cors+this.curl+this.team+'/'+idTeam, {headers: headers});
    }
}