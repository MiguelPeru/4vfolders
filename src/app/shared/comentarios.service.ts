import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable , of, throwError } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from '../../firebase/firebase.utils';
import { Comentario } from './comentario';

@Injectable({
    providedIn: 'root'
})
export class CommentsService{
    constructor(private firestore : AngularFirestore){}
   
    postComment(comment: Comentario){
        return this.firestore.collection('comments').add(comment);
    }
    getComments(): Observable<any>{
        return this.firestore.collection('comments').snapshotChanges();
    }
    
    comentarComentario(comment, id){
        return this.firestore.collection('comments').doc(id).update({
            subComments : firebase.firestore.FieldValue.arrayUnion(comment)
        })
    }
    borrarComentario(id){
        this.firestore.doc('comments/' + id).delete();
      }
    borrarSubComentario(comentario, subComentario){
        console.log(comentario.uid);
        console.log(subComentario.content)
        return this.firestore.collection('comments').doc(comentario.uid).update({
            subComments : firebase.firestore.FieldValue.arrayRemove(subComentario)
        }).catch(err =>{
            console.log(err)
        })
    }
}