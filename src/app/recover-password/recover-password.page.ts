import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../shared/authentication-service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.page.html',
  styleUrls: ['./recover-password.page.scss'],
})
export class RecoverPasswordPage implements OnInit {

  constructor(
    public authService: AuthenticationService,
    public router: Router,
    public alertCtrl: AlertController
  ) { }

  ngOnInit() {
  }

  recoverPassword(email){
    this.authService.PasswordRecover(email.value);
  }

}
