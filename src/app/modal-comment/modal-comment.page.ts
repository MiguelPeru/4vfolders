import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { CommentsService } from '../shared/comentarios.service';
import { Comentario } from '../shared/comentario';
import { AuthenticationService } from '../shared/authentication-service';
@Component({
  selector: 'app-modal-comment',
  templateUrl: './modal-comment.page.html',
  styleUrls: ['./modal-comment.page.scss'],
})
export class ModalCommentPage implements OnInit {
  public comment: any;
  slidesOptions = {
    initialSlide: 1,
    direction: 'horizontal',
    speed: 300,
    effect: 'slide',
    spaceBetween: 8,
    slidesPerView: 1.5,
    freeMode: true,
    loop: true
  };
  public content: string;
  constructor(private modal: ModalController, private navParams: NavParams, private commentService: CommentsService, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.comment = this.navParams.get('comment');
    console.log(this.comment)
  }
  dismissModal(){
    this.modal.dismiss();
  }
  enviarComentario(){
    let comentario: any;
    comentario = {
      userName: this.authenticationService.userData.displayName,
      content: this.content,
      date: new Date().getTime(),
      userId: this.authenticationService.userData.uid,
      subComments: [],
    }
    this.commentService.comentarComentario(comentario, this.comment.uid);
    this.modal.dismiss();
  }

}
