import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TablonPageRoutingModule } from './tablon-routing.module';

import { TablonPage } from './tablon.page';
import { ComentariosComponent } from '../components/comentarios/comentarios.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TablonPageRoutingModule
  ],
  declarations: [TablonPage, ComentariosComponent]
})
export class TablonPageModule {}
