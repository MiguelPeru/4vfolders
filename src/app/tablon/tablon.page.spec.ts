import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TablonPage } from './tablon.page';

describe('TablonPage', () => {
  let component: TablonPage;
  let fixture: ComponentFixture<TablonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablonPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TablonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
