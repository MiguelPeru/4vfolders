import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'leyes',
    loadChildren: () => import('./leyes/leyes.module').then( m => m.LeyesPageModule)
  },
  {
    path: 'condiciones',
    loadChildren: () => import('./condiciones/condiciones.module').then( m => m.CondicionesPageModule)
  },
  {
    path: 'sign-in-and-sign-up',
    loadChildren: () => import('./sign-in-and-sign-up/sign-in-and-sign-up.module').then( m => m.SignInAndSignUpPageModule), canActivate: [ AuthGuard ]
  },
  {
    path: 'verify-email',
    loadChildren: () => import('./verify-email/verify-email.module').then( m => m.VerifyEmailPageModule)
  },
  {
    path: 'buscarapi',
    loadChildren: () => import('./buscarapi/buscarapi.module').then( m => m.BuscarapiPageModule)
  },
  {
    path: 'recover-password',
    loadChildren: () => import('./recover-password/recover-password.module').then( m => m.RecoverPasswordPageModule)
  },
  {
    path: 'modal-comment',
    loadChildren: () => import('./modal-comment/modal-comment.module').then( m => m.ModalCommentPageModule)
  },
  {
    path: 'tablon',
    loadChildren: () => import('./tablon/tablon.module').then( m => m.TablonPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
