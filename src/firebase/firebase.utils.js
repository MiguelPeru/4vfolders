import firebase from 'firebase/app';

import 'firebase/firestore';

import 'firebase/auth';

const config = {
    apiKey: "AIzaSyBgqUHwEkJZsshi46RiooWnlMGf8VkSIf0",
    authDomain: "vfolders-36f66.firebaseapp.com",
    databaseURL: "https://vfolders-36f66.firebaseio.com",
    projectId: "vfolders-36f66",
    storageBucket: "vfolders-36f66.appspot.com",
    messagingSenderId: "877919150407",
    appId: "1:877919150407:web:46ad0364dde15913752c62",
    measurementId: "G-W2ZW1Q9C44"
  };

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();
const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });

export const signInWithGoogle = () => auth.signInWithPopup(provider);

export const loggedIn = () => auth.onAuthStateChanged(user => {
    if (user){
        return true
    } else {
        return false
    }
})

export default firebase;